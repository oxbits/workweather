import json
from urllib.request import urlopen

from django.http import HttpResponse, QueryDict

def forecast(request):

    query_string = QueryDict(request.META["QUERY_STRING"])

    lat = query_string.get('lat')
    long = query_string.get('long')

    top_resp = urlopen(f"https://api.weather.gov/points/{lat},{long}").read()

    top_json = json.loads(top_resp)

    forecast_resp = urlopen(top_json["properties"]["forecast"]).read()

    forecast_json = json.loads(forecast_resp)

    response = json.dumps(forecast_json["properties"]["periods"])

    return HttpResponse(response)
