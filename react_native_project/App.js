import * as React from 'react';
import {
  Button,
  Dimensions,
  Image,
  View,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  Platform,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Constants from 'expo-constants';


class HomeScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      latitude: '',
      longitude: '',
      data: []
    };
  }

  resize = () => this.forceUpdate()

  componentDidMount() {
    window.addEventListener('resize', this.resize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize)
  }

  makeRemoteRequest = () => {

    fetch(
      'http://127.0.0.1:8000/?lat=' + this.state.latitude + '&long=' + this.state.longitude,
      { mode: 'cors' }
    ).then(res => res.json())
      .then(res => {
        this.setState({
          data: res
        });
      })
  };

  render() {
    return (

      <View style={{
        width: "100%", height: "100%",
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <FlatList
          style={{
            width: Math.round(Math.min(Dimensions.get('window').width, Dimensions.get('window').height))
          }}
          data={this.state.data}
          ListHeaderComponent={
            <View style={{
              width: "100%",
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#fff',
              paddingHorizontal: 0,
              paddingTop: Math.round(
                Math.min(Dimensions.get('window').width, Dimensions.get('window').height) * 0.03
              ),
              paddingBottom: Math.round(
                Math.min(Dimensions.get('window').width, Dimensions.get('window').height) * 0.02
              ),
              marginVertical: "2%",
            }}>
              <Text style={[
                styles.title,
                {
                  maxWidth: Math.round(
                    ((Math.min(Dimensions.get('window').width, Dimensions.get('window').height) / 16) * 0.94) * 16
                  ),
                }
              ]}>Latitude:</Text>

              <TextInput
                style={{
                  height: 40, paddingLeft: 6, borderColor: 'gray', borderWidth: 1, color: "#000", placeholderTextColor: "#777"
                }}
                onChangeText={(latitude) => this.setState({ latitude })}
                value={this.state.latitude}
                width='67%'
                autoCapitalize='none'
                autoCorrect={false}
              /><Text style={[
                styles.title,
                {
                  maxWidth: Math.round(
                    ((Math.min(Dimensions.get('window').width, Dimensions.get('window').height) / 16) * 0.94) * 16
                  ),
                }
              ]}>Longitude:</Text>

              <TextInput
                style={{
                  height: 40, paddingLeft: 6, borderColor: 'gray', borderWidth: 1, color: "#000", placeholderTextColor: "#777"
                }}
                onChangeText={(longitude) => this.setState({ longitude })}
                value={this.state.longitude}
                width='67%'
                autoCapitalize='none'
                autoCorrect={false}
              />
              <View style={{
                  height: 20}} />
              <Button
                title='GET WEATHER'
                onPress={() => { this.makeRemoteRequest() }}
              />
            </View>
          }
          renderItem={({ item }) =>
            <View style={{
              width: "100%",
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#fff',
              paddingHorizontal: 0,
              paddingTop: Math.round(
                Math.min(Dimensions.get('window').width, Dimensions.get('window').height) * 0.03
              ),
              paddingBottom: Math.round(
                Math.min(Dimensions.get('window').width, Dimensions.get('window').height) * 0.02
              ),
              marginVertical: "2%",
            }}>
              <Text style={[
                styles.title,
                {
                  maxWidth: Math.round(
                    ((Math.min(Dimensions.get('window').width, Dimensions.get('window').height) / 16) * 0.94) * 16
                  ),
                }
              ]}>{item.name}</Text>
              <Text style={[
                styles.title,
                {
                  maxWidth: Math.round(
                    ((Math.min(Dimensions.get('window').width, Dimensions.get('window').height) / 16) * 0.94) * 16
                  ),
                }
              ]}>{item.shortForecast}</Text>
              <Image
                style={{
                  width: Math.round(
                    ((Math.min(Dimensions.get('window').width, Dimensions.get('window').height) / 16) * 0.94) * 16
                  ),
                  height: Math.round(
                    ((Math.min(Dimensions.get('window').width, Dimensions.get('window').height) / 16) * 0.94) * 9
                  ),
                }}
                source={{ uri: item.icon.replace('medium', 'large') }}
              />
              <Text style={[
                styles.half_title,
                {
                  maxWidth: Math.round(
                    ((Math.min(Dimensions.get('window').width, Dimensions.get('window').height) / 16) * 0.94) * 16
                  ),
                }
              ]}>Temperature: {item.temperature}°{item.temperatureUnit}</Text>
              <Text style={[
                styles.half_title,
                {
                  maxWidth: Math.round(
                    ((Math.min(Dimensions.get('window').width, Dimensions.get('window').height) / 16) * 0.94) * 16
                  ),
                }
              ]}>Wind: {item.windSpeed} {item.windDirection}</Text>
              <Text style={[
                styles.copy,
                {
                  maxWidth: Math.round(
                    ((Math.min(Dimensions.get('window').width, Dimensions.get('window').height) / 16) * 0.94) * 16
                  ),
                }
              ]}>{item.detailedForecast}</Text>
            </View>
          }
          keyExtractor={item => item.number.toString()}
        />
      </View>

    );
  }
}

const Stack = createStackNavigator();

function RootStack() {
  return (
    <Stack.Navigator
      initialRouteName="WORK WEATHER FORECAST"
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen
        name="WORK WEATHER FORECAST"
        component={HomeScreen}
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return <NavigationContainer>
    <RootStack />
  </NavigationContainer>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: '#fff',
    padding: 0,
    marginVertical: "6%",
    marginHorizontal: "6%",
  },
  title: {
    fontSize: 26,
    marginVertical: "2%",
    marginHorizontal: "0%",
  },
  half_title: {
    fontSize: 22,
    marginVertical: "2%",
    marginHorizontal: "0%",
  },
  copy: {
    fontSize: 16,
    marginVertical: "2%",
    marginHorizontal: "0%",
  },
  marginz: {
    marginVertical: "2%",
    marginHorizontal: "0%",
  },
});
