# WORK WEATHER FORECAST LIVE IN PRODUCTION

The project is built using Django and React Native via Expo.

The API is deployed using AWS Lambda and API Gateway via zappa.

Here are some example calls to the API:

• Spivey’s Corner, NC:

https://b6nbngccoc.execute-api.us-east-1.amazonaws.com/dev/?lat=35.113800&long=-78.406710

• Goodfield, IL:

https://b6nbngccoc.execute-api.us-east-1.amazonaws.com/dev/?lat=40.627740&long=-89.274920

• Greenwood, MS:

https://b6nbngccoc.execute-api.us-east-1.amazonaws.com/dev/?lat=33.521469&long=-90.192848


The React Native Web mobile first frontend has been deployed to AWS S3 and can be accessed here:

https://workweather.s3.amazonaws.com/index.html

or

http://workweather.s3-website-us-east-1.amazonaws.com/

---

# LOCAL SETUP

```
git clone git@gitlab.com:oxbits/workweather.git

or

git clone https://gitlab.com/oxbits/workweather.git

cd workweather
```

## Django Project:

```
pyenv install 3.8.10

pyenv local 3.8.10

virtualenv -p python ./env

source ./env/bin/activate

cd django_project

pip install -r ./requirements.txt

python ./manage.py runserver
```




Here are some example calls to the API:

• Spivey’s Corner, NC:

http://localhost:8000/?lat=35.113800&long=-78.406710

• Goodfield, IL:

http://localhost:8000/?lat=40.627740&long=-89.274920

• Greenwood, MS:

http://localhost:8000/?lat=33.521469&long=-90.192848

(you could also use `127.0.0.1`)

---

## Expo React Native Project:

You must have npm installed.

```
cd react_native_project  # dir at top level of repo

npm install

expo start --web
```

The Expo React Native Web mobile first frontend should become available at:

http://localhost:19006/

or

http://127.0.0.1:19006/

after developer tools spin up here:

http://localhost:19002/

---

Keep in mind:
* AWS Lambda sometimes goes to sleep so the initial load may take a few extra seconds
* Please keep in mind that if you hammer the API endpoint, weather.gov may return an error.  After waiting a few moments it should resume functioning